function myFunction(elmnt, type) {
    sortListDir(type);
    elmnt = document.getElementById(elmnt);
    var current = getCurrentRotation(elmnt);
    if (current > 0) {
        elmnt.style.transform = 'rotate(' + 0 + 'deg)';
    } else {
        elmnt.style.transform = 'rotate(' + 180 + 'deg)';
    }
}

function getCurrentRotation(el) {
    var st = window.getComputedStyle(el, null);
    var tm = st.getPropertyValue('transform') ||
        'none';
    if (tm !== 'none') {
        var values = tm.split('(')[1].split(')')[0].split(',');
        var angle = Math.round(Math.atan2(values[1], values[0]) * (180 / Math.PI));
        return (angle < 0 ? angle + 360 : angle);
    }
    return 0;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function animate(b) {
    var i;
    for (i = 0; i < b.length; i++) {
        b[i].classList.remove('show-anim');
        b[i].classList.add('hide-anim');
    }
}

async function removeAnimate(b) {
    var i;
    for (i = 0; i < b.length; i++) {
        b[i].classList.remove('hide-anim');
        b[i].classList.add('show-anim');
    }
}


async function sortListDir(type) {
    var i, switching, b, shouldSwitch, dir, switchcount = 0;
    switching = true;
    b = document.getElementsByClassName('test');
    animate(b).then();
    await sleep(250);
    removeAnimate(b).then();
    dir = 'asc';
    while (switching) {
        switching = false;
        for (i = 0; i < (b.length - 1); i++) {
            shouldSwitch = false;
            b = document.getElementsByClassName('test');
            if (dir === 'asc') {
                if (b[i].getElementsByClassName(type)[0].innerHTML.toLowerCase()
                    > b[i + 1].getElementsByClassName(type)[0].innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            } else if (dir === 'desc') {
                if (b[i].getElementsByClassName(type)[0].innerHTML.toLowerCase() < b[i + 1].getElementsByClassName(type)[0].innerHTML.toLowerCase()) {
                    shouldSwitch = true;
                    break;
                }
            }
        }
        if (shouldSwitch) {
            b[i].parentNode.insertBefore(b[i + 1], b[i]);
            switching = true;
            switchcount++;
        } else {
            if (switchcount === 0 && dir === 'asc') {
                dir = 'desc';
                switching = true;
            }
        }
    }
}
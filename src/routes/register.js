var express = require('express');
var service = require('../service');
var router = express.Router();

router.get('/register', function (req, res, next) {
    res.render('register', {
        title: 'Register',
        success: true
    });
});

module.exports = router;

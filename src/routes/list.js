var express = require('express');
var service = require('../service');
var router = express.Router();

let currentJson;

router.post('/list', function (req, res, next) {
    service.login(req.body.login, req.body.password)
        .then(json => {
            currentJson = json;
            service.showList(currentJson, res, 'login');
        });
});
router.post('/list_reg', function (req, res, next) {
    service.login(req.body.login, req.body.password)
        .then(json => {
            currentJson = json;
            service.showList(currentJson, res, 'register');
        });
});
module.exports = router;

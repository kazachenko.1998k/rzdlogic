var express = require('express');
var service = require('../service');
var router = express.Router();

router.get('/', function (req, res, next) {
    res.render('login', {
        title: 'Login',
        success: true
    });
});

module.exports = router;

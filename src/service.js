var fetch = require('node-fetch');

function checkStatus(res) {
    if (res.ok) {
        return res.text();
    }
    return res.json().then((json) => {
        throw Error(json.message);
    });
}

function parseJSON(text) {
    return text ? JSON.parse(text) : {};
}

function wrapFetch(url, options, contentType = 'application/json; charset=utf-8') {
    const headers = {};
    if (contentType !== null) {
        headers['Content-Type'] = contentType;
        headers.Accept = contentType;
    }
    return fetch(url, {
        headers,
        ...options,
    }).then(checkStatus)
        .then(parseJSON)
        .catch(msg => console.log(msg));
}

function getUsers() {
    return wrapFetch(`https://reqres.in/api/users?page=2`, {
        method: 'GET',
    });
}

function register(email, password) {
    return wrapFetch(`https://reqres.in/api/register`, {
            method: 'POST',
            body: JSON.stringify({email: email, password: password})
        }
    );
}

function login(email, password) {
    return wrapFetch(`https://reqres.in/api/login`, {
            method: 'POST',
            body: JSON.stringify({email: email, password: password})
        }
    );
}

function showList(json, res, back) {
    if (json == null) {
        res.render(back, {
            title: 'Ошибка входа',
            success: false
        });
    } else {
        service.getUsers()
            .then(json2 => {
                const data = json2.data;
                res.render('list', {
                    title: 'List users',
                    list: data
                });
            });
    }
}

const service = {
    getUsers: getUsers,
    register: register,
    login: login,
    showList: showList
};

module.exports = service;